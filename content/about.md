---
title: "About"
date: 2021-04-07T20:59:53-07:00
layout: staticpage
draft: false 
---

She11Co11ector is an Information Security professional and pentesting enthusiast
working toward an OSCP certification. 

[GPG key](/she11co11ector.pubkey)




"Brown Sea Shells on Brown Sand" photo by [Selcuk Teke](https://www.pexels.com/@selcuk-teke-393955)
